"""Library to represent MR Discussions."""
from dataclasses import InitVar
from dataclasses import dataclass
from datetime import datetime

from .users import User
from .users import UserCache


@dataclass
class Note:
    """A simple class to hold a Note."""

    id: str  # pylint: disable=invalid-name
    author: User | dict
    body: str
    system: bool
    updatedAt: str  # pylint: disable=invalid-name
    user_cache: InitVar[UserCache]

    def __post_init__(self, user_cache):
        """Set it up."""
        if isinstance(self.author, dict):
            self.author = user_cache.get(self.author)

    @property
    def timestamp(self):
        """Return the updatedAt as a datetime object."""
        return datetime.fromisoformat(self.updatedAt[:19])


@dataclass
class Discussion:
    """A simple class to hold a Discussion."""

    resolvable: bool
    resolved: bool
    notes: list[Note] | dict
    user_cache: InitVar[UserCache]

    def __post_init__(self, user_cache):
        """Set up a new Discussion."""
        if isinstance(self.notes, dict):
            self.notes = [Note(**note, user_cache=user_cache) for note in self.notes['nodes']]
