# Subsystems Webhook

## Purpose

This webhook identifies the subsystems that a Merge Request's changeset
modifies and adds a "Subsystem:" specific label. It also adds an ExternalCI::
label that is set to ExternalCI::OK when all external testing requirements
indicated by owners.yaml are met.

## Notifications

This webhook will post a notification report with the subscribing people
tagged to relevant Merge Requests.  Any updates to those subscriptions
will be delivered via edits to that same comment. For more information about
Subsystem subscribers, visit the [kernel-watch project](https://gitlab.com/redhat/rhel/src/kernel/watch.git)

## Manual Runs

You can run the webhook manually on a merge request URL with the command:

    python3 -m webhook.subsystems \
        --disable-inactive-branch-check \
        --merge-request https://gitlab.com/group/repo/-/merge_requests/1 \
        --owners-yaml owners.yml \
        --local-repo-path /data/kernel-watch

The `KERNEL_WATCH_URL` environment variable is expected to be set and be a string
with the URL of the kernel-watch project.

The [main README](README.md#running-a-webhook-for-one-merge-request) describes
some common environment variables that can be set that are applicable for all
webhooks.

The `--map-file` argument can also be passed to the script via the `MAP_FILE`
environment variable and `--local-repo-path` with `LOCAL_REPO_PATH`.

## Reporting

- Label prefix: `ExternalCI::`
- Label prefix: `Subsystem:`

The webhook reports the overall result of the check by applying a scoped label
to the MR with the prefix `ExternalCI::`. There may be additional double-scoped
`ExternalCI::<sst>::<status>` labels created if the MR files touch areas defined
in owners.yaml that have readyForMergeDeps set. The corresponding SST teams will
manage these labels and set them to `OK` when their testing is complete or `Waived`
if testing will be skipped.
